from django.urls import path
from accounts.views import signup, user_login, logout_

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_, name="logout"),
    path("signup/", signup, name="signup"),
]

from django.urls import path
from receipts.views import (
    create_receipt,
    receipt_show,
    category_list,
    create_account,
    account_list,
    create_category,
)

urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_show, name="home"),
]

from django.contrib import admin
from .models import Receipt, Account, ExpenseCategory

# Register your models here.


@admin.register(Receipt)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
    list_filter = (
        "purchaser",
        "category",
        "account",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "number",
        "owner",
    )
    list_filter = ("owner",)


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "owner",)
    list_filter = ("owner",)
